package bitrix

import (
	"context"
	"fmt"
	genBitrix "gitlab.com/feoktistov_av/bitrix-client/gen"
	"net/http"
)

type PatchPosOrderStatusRequest struct {
	OrderId string `json:"order_id"`
	Status  string `json:"status"`
}

func (c *Client) PatchPosOrderStatus(ctx context.Context, body PatchPosOrderStatusRequest) error {
	response, err := c.client.PatchApiSalePosOrderStatusWithResponse(
		ctx,
		genBitrix.PatchApiSalePosOrderStatusJSONRequestBody{
			OrderId: body.OrderId,
			Status:  body.Status,
		},
	)
	if err != nil {
		if response == nil {
			return Error{Err: fmt.Errorf("%w: %s", ErrInternal, err), Name: fmt.Sprintf("PatchPosOrderStatus %s", body.OrderId)}
		}
		return Error{Err: err, StatusCode: response.StatusCode(), Name: fmt.Sprintf("PatchPosOrderStatus %s", body.OrderId), ResponseBody: response.Body}
	}

	switch status := response.StatusCode(); status {
	case http.StatusOK:
		return nil

	case http.StatusBadRequest:
		err = Error{Err: ErrBadRequest, Name: fmt.Sprintf("PatchPosOrderStatus %s", body.OrderId), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return err
	case http.StatusUnauthorized:
		err = Error{Err: ErrUnauthorizedRequest, Name: fmt.Sprintf("PatchPosOrderStatus %s", body.OrderId), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return err
	case http.StatusUnprocessableEntity:
		err = Error{Err: ErrUnprocessableRequest, Name: fmt.Sprintf("PatchPosOrderStatus %s", body.OrderId), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return err
	case http.StatusInternalServerError:
		err = Error{Err: ErrInternalServer, Name: fmt.Sprintf("PatchPosOrderStatus %s", body.OrderId), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return err

	default:
		err = Error{Err: ErrUnknown, Name: fmt.Sprintf("PatchPosOrderStatus %s", body.OrderId), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return err
	}
}
