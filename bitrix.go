package bitrix

import (
	"context"
	"net/http"

	genBitrix "gitlab.com/feoktistov_av/bitrix-client/gen"

	"gitlab.com/sprinttechnologies/libs/golibs/magnit/metrics"
)

type RequestEditorFn func(ctx context.Context, req *http.Request) error

type ResponseEditorFn func(ctx context.Context, res *http.Response) error

type HTTPRequestDoer interface {
	Do(req *http.Request) (*http.Response, error)
}

type Client struct {
	client genBitrix.ClientWithResponses
}

type ClientOptions struct {
	Server          string
	ClientName      string
	ServiceName     string
	Metrics         *metrics.PrometheusClient
	RequestEditors  []RequestEditorFn
	ResponseEditors []ResponseEditorFn
	HTTPClient      HTTPRequestDoer
}

func New(opt *ClientOptions) (*Client, error) {
	genClient, err := genBitrix.NewClient(
		opt.Server,
		genBitrix.WithHTTPClient(opt.HTTPClient),
		genBitrix.WithMetrics(opt.Metrics),
		genBitrix.WithClientName(opt.ClientName),
		genBitrix.WithServiceName(opt.ServiceName),
	)
	if err != nil {
		return nil, err
	}

	for _, reqE := range opt.RequestEditors {
		if err := genBitrix.WithRequestEditorFn(genBitrix.RequestEditorFn(reqE))(genClient); err != nil {
			return nil, err
		}
	}

	for _, resE := range opt.ResponseEditors {
		if err := genBitrix.WithResponseEditorFn(genBitrix.ResponseEditorFn(resE))(genClient); err != nil {
			return nil, err
		}
	}

	return &Client{client: genBitrix.ClientWithResponses{ClientInterface: genClient}}, nil
}
