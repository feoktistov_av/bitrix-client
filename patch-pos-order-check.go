package bitrix

import (
	"context"
	"fmt"
	genBitrix "gitlab.com/feoktistov_av/bitrix-client/gen"
	"net/http"
)

type PatchPosOrderCheckRequest struct {
	OrderId           string                              `json:"order_id"`
	CartItems         []PatchPosOrderCheckRequestCartItem `json:"cart_items"`
	DicountFz         *float32                            `json:"dicount_fz,omitempty"`
	DiscountSum       *float32                            `json:"discount_sum,omitempty"`
	MarketingDiscount *float32                            `json:"marketing_discount,omitempty"`
	TotalSum          float32                             `json:"total_sum"`
	Type              string                              `json:"type"`
}

type PatchPosOrderCheckRequestCartItem struct {
	Code          string  `json:"code"`
	Price         float32 `json:"price"`
	PriceDiscount float32 `json:"price_discount"`
	Quantity      float32 `json:"quantity"`
	TaxValue      float32 `json:"tax_value"`
}

func (c *Client) PatchPosOrderCheck(ctx context.Context, body PatchPosOrderCheckRequest) error {
	var items []genBitrix.PatchPosOrderCheckRequestCartItem

	for _, i := range body.CartItems {
		ci := genBitrix.PatchPosOrderCheckRequestCartItem{
			Code:          i.Code,
			Price:         i.Price,
			PriceDiscount: i.PriceDiscount,
			Quantity:      i.Quantity,
			TaxValue:      i.TaxValue,
		}
		items = append(items, ci)
	}

	response, err := c.client.PatchApiSaleCheckdbOrderWithResponse(
		ctx,
		genBitrix.PatchApiSaleCheckdbOrderJSONRequestBody{
			OrderId:           body.OrderId,
			CartItems:         items,
			DicountFz:         body.DicountFz,
			DiscountSum:       body.DiscountSum,
			MarketingDiscount: body.MarketingDiscount,
			TotalSum:          body.TotalSum,
			Type:              body.Type,
		},
	)
	if err != nil {
		if response == nil {
			return Error{Err: fmt.Errorf("%w: %s", ErrInternal, err), Name: fmt.Sprintf("PatchPosOrderCheck %s", body.OrderId)}
		}
		return Error{Err: err, StatusCode: response.StatusCode(), Name: fmt.Sprintf("PatchPosOrderCheck %s", body.OrderId), ResponseBody: response.Body}
	}

	switch status := response.StatusCode(); status {
	case http.StatusOK:
		return nil

	case http.StatusBadRequest:
		err = Error{Err: ErrBadRequest, Name: fmt.Sprintf("PatchPosOrderCheck %s", body.OrderId), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return err
	case http.StatusUnauthorized:
		err = Error{Err: ErrUnauthorizedRequest, Name: fmt.Sprintf("PatchPosOrderCheck %s", body.OrderId), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return err
	case http.StatusUnprocessableEntity:
		err = Error{Err: ErrUnprocessableRequest, Name: fmt.Sprintf("PatchPosOrderCheck %s", body.OrderId), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return err
	case http.StatusInternalServerError:
		err = Error{Err: ErrInternalServer, Name: fmt.Sprintf("PatchPosOrderCheck %s", body.OrderId), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return err

	default:
		err = Error{Err: ErrUnknown, Name: fmt.Sprintf("PatchPosOrderCheck %s", body.OrderId), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return err
	}
}
