package bitrix

import (
	"errors"
	"fmt"
)

var (
	ErrInternal             = errors.New("error before send request")
	ErrUnknown              = errors.New("unknown bitrix error")
	ErrUnauthorizedRequest  = errors.New("unauthorized bitrix error")
	ErrBadRequest           = errors.New("bad request bitrix error")
	ErrUnprocessableRequest = errors.New("bitrix logical error")
	ErrInternalServer       = errors.New("internal server error")
)

type Error struct {
	Err          error
	Name         string
	StatusCode   int
	ResponseBody []byte
}

func (ce Error) Error() string {
	if ce.StatusCode != 0 {
		return fmt.Sprintf("method: %s; status_code: %d; error: %s; response_body: %s",
			ce.Name, ce.StatusCode, ce.Err.Error(), string(ce.ResponseBody))
	}
	return fmt.Sprintf("method: %s; error: %s", ce.Name, ce.Err.Error())
}

func (ce Error) Unwrap() error {
	return ce.Err
}
