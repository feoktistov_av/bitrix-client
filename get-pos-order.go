package bitrix

import (
	"context"
	"fmt"
	genBitrix "gitlab.com/feoktistov_av/bitrix-client/gen"
	"net/http"
)

type GetPosOrderRequest struct {
	PickupBarcode string `json:"pickup_barcode"`
	Wscode        string `json:"wscode"`
}

func (c *Client) GetPosOrder(ctx context.Context, body GetPosOrderRequest) (*genBitrix.GetPosOrderResponse, error) {
	response, err := c.client.GetApiSalePosOrderWithResponse(
		ctx,
		genBitrix.GetApiSalePosOrderJSONRequestBody{
			PickupBarcode: body.PickupBarcode,
			Wscode:        body.Wscode,
		},
	)
	if err != nil {
		if response == nil {
			return nil, Error{Err: fmt.Errorf("%w: %s", ErrInternal, err), Name: fmt.Sprintf("GetPosOrder %s %s", body.PickupBarcode, body.Wscode)}
		}
		return nil, Error{Err: err, StatusCode: response.StatusCode(), Name: fmt.Sprintf("GetPosOrder %s %s", body.PickupBarcode, body.Wscode), ResponseBody: response.Body}
	}

	switch status := response.StatusCode(); status {
	case http.StatusOK:
		return response.JSON200, nil

	case http.StatusBadRequest:
		err = Error{Err: ErrBadRequest, Name: fmt.Sprintf("GetPosOrder %s %s", body.PickupBarcode, body.Wscode), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return nil, err
	case http.StatusUnauthorized:
		err = Error{Err: ErrUnauthorizedRequest, Name: fmt.Sprintf("GetPosOrder %s %s", body.PickupBarcode, body.Wscode), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return nil, err
	case http.StatusUnprocessableEntity:
		err = Error{Err: ErrUnprocessableRequest, Name: fmt.Sprintf("GetPosOrder %s %s", body.PickupBarcode, body.Wscode), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return nil, err
	case http.StatusInternalServerError:
		err = Error{Err: ErrInternalServer, Name: fmt.Sprintf("GetPosOrder %s %s", body.PickupBarcode, body.Wscode), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return nil, err

	default:
		err = Error{Err: ErrUnknown, Name: fmt.Sprintf("GetPosOrder %s %s", body.PickupBarcode, body.Wscode), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return nil, err
	}
}
