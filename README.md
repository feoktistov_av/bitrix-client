# bitrix client
#### Клиент для интеграции с API монолита (Bitrix)

### Пример использования

```go
package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/feoktistov_av/bitrix-client"
)

func main() {
	httpClient := http.Client{}

	client, err := bitrix.New(&bitrix.ClientOptions{
		Server:      "https://apteka-stage.magnit.ru",
		ClientName:  "bitrix",
		ServiceName: "ms",
		Hostname:    "localhost",
		HTTPClient:  &httpClient,
		Metrics:     nil, // CHANGEME!
	})
	if err != nil {
		panic(err)
	}

	//TODO

	panic(err)
}

```


## Генерация кода

Генерация кода выполняется с помощью docker<br>
Пример запуска есть в `Makefile`
