.PHONY: docker-codegen

VERSION=v0.1.0
CLIENT_NAME=bitrix
PACKAGE_NAME=Bitrix

codegen:
	@mkdir -p gen
	oapi-codegen -old-config-style --templates /app/templates/oapi-codegen/ --package "gen${PACKAGE_NAME}" --generate types -o "gen/types.go" "api/openapi.yaml";
	oapi-codegen -old-config-style --templates /app/templates/oapi-codegen/ --package "gen${PACKAGE_NAME}" --generate client -o "gen/client.go" "api/openapi.yaml";


docker-codegen:
	docker build -t customer-codegen -f build/Dockerfile --progress plain --output ./ .

lint:
	golangci-lint run -c .golangci.yml