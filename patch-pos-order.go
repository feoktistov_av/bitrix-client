package bitrix

import (
	"context"
	"fmt"
	genBitrix "gitlab.com/feoktistov_av/bitrix-client/gen"
	"net/http"
)

type PatchPosOrderRequest struct {
	OrderId   string                         `json:"order_id"`
	CartItems []PatchPosOrderRequestCartItem `json:"cart_items"`
	Wscode    string                         `json:"wscode"`
}

type PatchPosOrderRequestCartItem struct {
	Cis      *string `json:"cis,omitempty"`
	Code     string  `json:"code"`
	Excise   *string `json:"excise,omitempty"`
	Id       string  `json:"id"`
	Quantity float32 `json:"quantity"`
}

func (c *Client) PatchPosOrder(ctx context.Context, body PatchPosOrderRequest) (*genBitrix.PatchPosOrderResponse, error) {
	var items []genBitrix.PatchPosOrderRequestCartItem

	for _, i := range body.CartItems {
		ci := genBitrix.PatchPosOrderRequestCartItem{
			Cis:      i.Cis,
			Code:     i.Code,
			Excise:   i.Excise,
			Id:       i.Id,
			Quantity: i.Quantity,
		}
		items = append(items, ci)
	}

	response, err := c.client.PatchApiSalePosOrderWithResponse(
		ctx,
		genBitrix.PatchApiSalePosOrderJSONRequestBody{
			OrderId:   body.OrderId,
			CartItems: items,
			Wscode:    body.Wscode,
		},
	)
	if err != nil {
		if response == nil {
			return nil, Error{Err: fmt.Errorf("%w: %s", ErrInternal, err), Name: fmt.Sprintf("PatchPosOrder %s", body.OrderId)}
		}
		return nil, Error{Err: err, StatusCode: response.StatusCode(), Name: fmt.Sprintf("PatchPosOrder %s", body.OrderId), ResponseBody: response.Body}
	}

	switch status := response.StatusCode(); status {
	case http.StatusOK:
		return response.JSON200, nil

	case http.StatusBadRequest:
		err = Error{Err: ErrBadRequest, Name: fmt.Sprintf("PatchPosOrder %s", body.OrderId), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return nil, err
	case http.StatusUnauthorized:
		err = Error{Err: ErrUnauthorizedRequest, Name: fmt.Sprintf("PatchPosOrder %s", body.OrderId), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return nil, err
	case http.StatusUnprocessableEntity:
		err = Error{Err: ErrUnprocessableRequest, Name: fmt.Sprintf("PatchPosOrder %s", body.OrderId), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return nil, err
	case http.StatusInternalServerError:
		err = Error{Err: ErrInternalServer, Name: fmt.Sprintf("PatchPosOrder %s", body.OrderId), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return nil, err

	default:
		err = Error{Err: ErrUnknown, Name: fmt.Sprintf("PatchPosOrder %s", body.OrderId), StatusCode: response.StatusCode(), ResponseBody: response.Body}
		return nil, err
	}
}
